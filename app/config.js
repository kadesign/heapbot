const Telegraf = require('telegraf');
const SocksAgent = require('socks5-https-client/lib/Agent');

let botOpts;
if (process.env.NODE_ENV === 'development' && process.env.DEV_PROXY_HOST && process.env.DEV_PROXY_PORT) {
  const proxyAgentOpts = {
    socksHost: process.env.DEV_PROXY_HOST,
    socksPort: process.env.DEV_PROXY_PORT
  };

  if (process.env.DEV_PROXY_USER) proxyAgentOpts.socksUser = process.env.DEV_PROXY_USER;
  if (process.env.DEV_PROXY_PASSWORD) proxyAgentOpts.socksPassword = process.env.DEV_PROXY_PASSWORD;

  botOpts = {
    username: process.env.BOT_NAME,
    telegram: {
      agent: new SocksAgent(proxyAgentOpts)
    }
  };
} else {
  botOpts = {
    username: process.env.BOT_NAME
  };
}

module.exports.bot = new Telegraf(process.env.BOT_TOKEN, botOpts);

module.exports.botCommands = {
  getStickerInfo: 'stickerinfo',
  leaveScene: 'cancel',
  help: 'help'
};
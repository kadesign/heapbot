module.exports.to = function (promise) {
  return promise.then(data => {
    return [null, data];
  })
    .catch(err => [err]);
};

module.exports.getRandomIndex = arrayLength => {
  return Math.floor(Math.random() * arrayLength);
};

module.exports.getRandomBetween = (a, b) => {
  return a + Math.floor(Math.random() * b);
};

module.exports.escapeUnderscore = string => {
  if (string.indexOf('_') >= 0) {
    string = string.replace(/_/g, '\\_');
  }

  return string;
};
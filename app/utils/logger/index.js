module.exports = {
  log(msg) {
    console.log(this.getCurrentTimestamp() + ' [ debug ] ' + msg);
  },

  warn(msg) {
    console.log(this.getCurrentTimestamp() + ' [warning] ' + msg);
  },

  error(err) {
    console.log(this.getCurrentTimestamp() + ' [ error ] ' + err.message);
    console.log(err.stack);
  },

  logSentMessage(chatId, msg) {
    this.log('Message ' + msg + ' was sent to user ' + chatId);
  },

  getCurrentTimestamp() {
    const date = new Date();
    return date.getUTCFullYear() + '-' + (('0' + (date.getUTCMonth() + 1)).slice(-2)) + '-' +
      (('0' + date.getUTCDate()).slice(-2)) + ' ' + (('0' + date.getUTCHours()).slice(-2)) + ':' +
      (('0' + date.getUTCMinutes()).slice(-2)) + ':' + (('0' + date.getUTCSeconds()).slice(-2));
  }
};
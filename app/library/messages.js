const {botCommands} = require('../config');

module.exports = {
  scenes: {
    getStickerInfo: {
      enter: 'Отправь мне стикер, о котором хочешь получить информацию.\n' +
        '_С анимированными стикерами это пока не работает!_',
      response: '*Стикерпак:* [%s](https://t.me/addstickers/%s)\n' +
        '*Эмодзи:* %s\n' +
        '*File ID:* %s',
      error: 'Отправь *стикер* или /' + botCommands.leaveScene + ', чтобы отменить операцию.'
    },
    leave: 'Текущая операция отменена.'
  },
  commands: {
    help: '*Команды:*\n\n' +
      '/' + botCommands.help + ' - список команд'
  }
};
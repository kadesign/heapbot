require('dotenv').config();

// External libs
if (process.env.SENTRY_DSN) {
  const Sentry = require('@sentry/node');
  Sentry.init({
    environment: process.env.NODE_ENV ? process.env.NODE_ENV : undefined
  });
}
const Extra = require('telegraf/extra');
const session = require('telegraf/session');
const Stage = require('telegraf/stage');

// Internal libs
const Logger = require('./utils/logger');
const Messages = require('./library/messages');
const {to} = require('./utils/functions');

// Components
const init = require('./components/initializer');
const {names, scenes} = require('./components/scenes');

// Bot initialization
const {bot, botCommands} = require('./config');

// Scene manager initialization
const stage = new Stage();
stage.command(botCommands.leaveScene, (ctx) => {
  ctx.scene.leave();
  sendMessage(ctx, Messages.scenes.leave, 'scenes.leave');
});

stage.register(...scenes);
bot.use(session());
bot.use(stage.middleware());

// Bot commands
const sendMessage = (ctx, msg, msgCode, markdown = false) => {
  ctx.telegram.sendMessage(ctx.message.chat.id, msg, (markdown ? Extra.markdown() : undefined))
    .then(() => {
      Logger.logSentMessage(ctx.message.chat.id, msgCode);
    });
};

bot.command(botCommands.getStickerInfo, ctx => ctx.scene.enter(names.getStickerInfo));

bot.command(botCommands.help, ctx => {
  sendMessage(ctx, Messages.commands.help, 'commands.help', true)
});

// Main workflow
async function main() {
  let [err] = await to(init.initialize());
  if (err) throw err;

  bot.startPolling();
}

main().catch(err => {
  Logger.error(err);
});
const util = require('util');
const Scene = require('telegraf/scenes/base');
const Extra = require('telegraf/extra');

const Logger = require('../utils/logger');
const Messages = require('../library/messages');
const { escapeUnderscore } = require('../utils/functions');

const sendMessage = (ctx, msg, msgCode, markdown = false) => {
  ctx.telegram.sendMessage(ctx.message.chat.id, msg, (markdown ? Extra.markdown() : undefined))
    .then(() => {
      Logger.logSentMessage(ctx.message.chat.id, msgCode);
    });
};

const sceneNames = {
  getStickerInfo: 'StickerInfo'
};

const sStickerInfo = () => {
  const scene = new Scene(sceneNames.getStickerInfo);

  scene.enter(ctx => {
    sendMessage(ctx, Messages.scenes.getStickerInfo.enter, 'scenes.getStickerInfo.enter', true);
  });
  scene.on(['sticker'], async ctx => {
    const set = await ctx.getStickerSet(ctx.message.sticker.set_name);
    const msg = util.format(Messages.scenes.getStickerInfo.response, escapeUnderscore(set.title), escapeUnderscore(ctx.message.sticker.set_name), ctx.message.sticker.emoji, escapeUnderscore(ctx.message.sticker.file_id));
    sendMessage(ctx, msg, 'scenes.getStickerInfo.response', true);
    ctx.scene.leave();
  });
  scene.on('message', ctx => {
    sendMessage(ctx, Messages.scenes.getStickerInfo.error, 'scenes.getStickerInfo.error', true);
  });

  return scene;
};

module.exports.scenes = [
  sStickerInfo()
];

module.exports.names = sceneNames;
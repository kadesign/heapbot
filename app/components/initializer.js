const {to} = require('../utils/functions');
const Logger = require('../utils/logger');

const {bot} = require('../config');

module.exports.initialize = async function () {
  let err, botInfo, startTime, endTime;

  Logger.log(process.env.BOT_NAME + ' started');
  Logger.log('Current version: ' + process.env.npm_package_version);

  Logger.log('Connectivity check started');

  startTime = new Date();
  [err, botInfo] = await to(bot.telegram.getMe());
  if (err) {
    if (err.code === 'ENOTFOUND' || err.code === 'ECONNRESET') throw new Error('Telegram API error: Not available');
    if (err.code === 401) throw new Error('Telegram API error: Invalid token');
    throw new Error('Telegram API error: ' + err.message);
  }
  if (botInfo) {
    endTime = new Date();
    Logger.log('Telegram API connection OK, ping: ' + (endTime - startTime).toString() + 'ms');
    if (process.env.BOT_NAME !== botInfo.username) throw new Error('Telegram API error: Wrong bot name configured');
    Logger.log('Telegram API token check OK');
  }

  Logger.log('Connectivity check finished');
};